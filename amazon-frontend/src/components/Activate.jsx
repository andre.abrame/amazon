import { CircularProgress } from '@mui/material';
import axios from 'axios';
import { useEffect, useState } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom';

function Activate() {
    const { token } = useParams();
    const [status, setStatus] = useState("loading");
    const navigate = useNavigate();

    useEffect(() => {
        axios.post(import.meta.env.VITE_BACKEND_URL + "/activate/" + token)
        .then(() => {
            setStatus("success");
            setTimeout(() => navigate("/login"), 3000);
          })
        .catch(() => setStatus("failed"))
    }, [token, navigate]);

    function render() {
        switch (status) {
            case "loading":
                return (<CircularProgress />)
            case "success":
                return (<>
                            <h2>Your account is validated !</h2>
                            <p>You will be redirected shortly to the <Link to={"/login"}>login</Link> page.</p>
                        </>)
            case "failed": 
                return (<h2>Oh no ! your token does not look like to be valid. Try again</h2>)
        }
    }

  return (
    <>
        {render()}
    </>
  )
}

export default Activate