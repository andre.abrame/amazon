import { Box, Button, TextField } from '@mui/material'
import { useContext, useState } from 'react'
import { Link } from 'react-router-dom';
import { AuthenticationContext } from '../App';

function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState();
    const [, login, ] = useContext(AuthenticationContext)

    const handleSubmit = (evt) => {
        evt.preventDefault();
        login(email, password)
          .catch(error => setErrors(error.message));
    }

  return (
    <>
    <h1>Login</h1>
    <form id="login-form" onSubmit={handleSubmit}>
        <TextField label="Email" name="email" value={email} variant="filled" onChange={evt => setEmail(evt.target.value)}/>
        <TextField type="password" label="Password" name="password" value={password} variant="filled" onChange={evt => setPassword(evt.target.value)}/>
        <div>{errors}</div>
        <Box sx={{display: "flex", flexDirection: "row", justifyContent: "space-around", margin: "0 auto", maxWidth: "500px"}}>
          <Link to="/"><Button type="button" variant="outlined">Cancel</Button></Link>
          <Button type="submit" variant="contained">Submit</Button>
        </Box>
      </form>
    </>
  )
}

export default Login