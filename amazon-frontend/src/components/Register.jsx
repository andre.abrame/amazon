import { Alert, Box, Button, CircularProgress, FormControl, InputLabel, MenuItem, Select, Snackbar, TextField } from '@mui/material';
import axios from 'axios';
import { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { useFormValidation } from '../shared/hooks/useFormValidation';
import SubmitButton from '../shared/components/SubmitButton';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';

function Register() {

    const navigate = useNavigate();
    const [user, setUser] = useState({ 
        prenom: "", 
        nom: "", 
        email: "", 
        password: "", 
        type: 'CLIENT' });
    const [validate, isTouched, isValid, getMessages, markAsTouched] = useFormValidation({});
    const [status, setStatus] = useState({icon: undefined, content: "submit"});
    const [open, setOpen] = useState(false);
    const [message, setMessage] = useState("");
    const [severity, setSeverity] = useState("info");

    function handleChange(event) {
        // retrieve the name and value of the changed input
        const { name, value } = event.target;
        const newUser = { ...user, [name]: value };
        // change state
        setUser(newUser);
        // apply validation rules
        validate(newUser, name);
    }

    function handleSubmit(event) {
        // displaying snackbar
        setStatus({ content: "saving", icon: <CircularProgress size={24} color="inherit" />})
        setMessage("save in progresss")
        setSeverity("info")
        setOpen(true);
        // disable default submit
        event.preventDefault();
        // check validation
        // send data to backend
        axios.post(import.meta.env.VITE_BACKEND_URL + "/register", user)
            .then(() => {
                setStatus({ content: "success", icon: <CheckCircleOutlineIcon />, color: "success"})
                // updating snackbar
                setMessage("save succeed");
                setSeverity("success");
                setTimeout(() => {
                  setOpen(false);
                  navigate("/products");
                }, 2500);
            })
            .catch((error) => {
              // updating snackbar
                setStatus({ content: "error", icon: <ErrorOutlineIcon />, color: "error"})
              setMessage("save failed : " + error);
              setSeverity("error");
              setTimeout(() => {
                setStatus({ content: "submit"})
                setOpen(false);
              }, 2500);
            })
    }

    function handleBlur(event) {
        // retrieve the name blurred input
        const { name } = event.target;
        // change its touched status
        markAsTouched(name);
    }

    useEffect(() => {
        validate(user);
    }, []);

    return (
        <>
            <h1>Register</h1>
            <form id="new-product-form" onSubmit={handleSubmit}>
                <TextField onBlur={handleBlur} error={isTouched("prenom") && !isValid("prenom")} helperText={getMessages("prenom")} label="Firstname" name="prenom" value={user.prenom} variant="filled" onChange={handleChange} />
                <TextField onBlur={handleBlur} error={isTouched("nom") && !isValid("nom")} helperText={getMessages("nom")} label="Lastname" name="nom" value={user.nom} variant="filled" onChange={handleChange} />
                <TextField onBlur={handleBlur} error={isTouched("email") && !isValid("email")} helperText={getMessages("email")} label="Email" name="email" value={user.email} variant="filled" onChange={handleChange} />
                <FormControl fullWidth variant="filled">
                    <InputLabel id="role-select">Role</InputLabel>
                    <Select
                        labelId="role-select"
                        name="type"
                        value={user.type}
                        label="Role"
                        onChange={handleChange}
                    >
                        <MenuItem value={"CLIENT"}>Client</MenuItem>
                        <MenuItem value={"VENDEUR"}>Seller</MenuItem>
                    </Select>
                </FormControl>
                <TextField type="password" onBlur={handleBlur} error={isTouched("password") && !isValid("password")} helperText={getMessages("password")} label="Password" name="password" value={user.password} variant="filled" onChange={handleChange} />
                <Box sx={{display: "flex", flexDirection: "row", justifyContent: "space-around", margin: "0 auto", maxWidth: "500px"}}>
                    <Link to="/products"><Button type="button" variant="outlined">Cancel</Button></Link>
                    <SubmitButton status={status} type="submit" variant="contained"  disabled={!isValid()}></SubmitButton>
                </Box>
            </form>
            <Snackbar open={open}>
                <Alert severity={severity} sx={{ width: '100%' }}>
                    {message}
                </Alert>
            </Snackbar>
        </>
    )
}

export default Register