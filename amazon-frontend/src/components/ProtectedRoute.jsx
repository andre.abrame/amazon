import { Navigate, Route } from 'react-router-dom';
import PropTypes from 'prop-types';



function ProtectedRoute({isAllowed, redirectPath = '/login', ...rest}) {
    if (!isAllowed)
        return <Navigate to={redirectPath} replace />;
    else
        return <Route {...rest}></Route>
}


ProtectedRoute.propTypes = {
    isAllowed: PropTypes.bool.isRequired,
    redirectPath: PropTypes.string
};

export default ProtectedRoute