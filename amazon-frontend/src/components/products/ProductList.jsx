import { useContext, useEffect, useState } from "react"
import axios from "axios"
import { Link, useNavigate } from "react-router-dom";
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import CircularProgress from '@mui/material/CircularProgress';
import { Button, Fab, InputAdornment, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from "@mui/material";
import { AuthenticationContext } from "../../App";


function ProductList() {
    const [authenticatedUser, ,] = useContext(AuthenticationContext);
    const [products, setProducts] = useState(undefined);
    const [count, setCount] = useState(0);
    const [query, setQuery] = useState("");
    const [queryLastTimeout, setQueryLastTimeout] = useState(undefined);
    const navigate = useNavigate();

    async function onQueryChange(evt) {
        const q = evt.target.value;
        setQuery(q);
        if (queryLastTimeout)
            clearTimeout(queryLastTimeout);
        setQueryLastTimeout(setTimeout(
            async () => {
                const response = await axios.get(`${import.meta.env.VITE_BACKEND_URL}/produits?nom=${q}`);
                setProducts(response.data.results);
                setCount(response.data.count);
                setQueryLastTimeout(undefined);
            },
            300));
    }

    function onLoadMore() {
        axios.get(`${import.meta.env.VITE_BACKEND_URL}/produits?page=${products.length / 5}&pageSize=5`).then((response) => {
            setProducts([...products, ...response.data.results]);
            setCount(response.data.count);
        });
    }

    function onAdd() {
        navigate("/products/new");
    }

    function onDelete(evt, id) {
        axios.delete(`${import.meta.env.VITE_BACKEND_URL}/produits/${id}`)
            .then(() => {
                alert("success :)");
                const index = products.findIndex(p => p.id === id);
                setProducts([
                    ...products.slice(0, index),
                    ...products.slice(index + 1)
                ]);
            })
            .catch(() => alert("error :("))
    }

    useEffect(() => {
        axios.get(import.meta.env.VITE_BACKEND_URL + "/produits?page=0&pageSize=5").then((response) => {
            response.data.results.forEach(p => p.createdAt = new Date(p.createdAt));
            setProducts(response.data.results);
            setCount(response.data.count);
        });
    }, []);

    return (
        <>
            {!products ? (
                <p><CircularProgress /> loading ...</p>
            ) : (
                <>
                    <h1>ProductList</h1>
                    <TextField
                        label="Search"
                        variant="outlined"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon />
                                </InputAdornment>
                            ),
                        }}
                        value={query}
                        onChange={onQueryChange}
                        sx={{width: "80%", margin: "0 auto"}}
                    />

                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="right">Id</TableCell>
                                    <TableCell>Image</TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Description</TableCell>
                                    <TableCell align="right">Price</TableCell>
                                    <TableCell align="right">Availability</TableCell>
                                    <TableCell align="right">Actions</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {products.map((p) => (
                                    <TableRow key={p.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="right" component="th" scope="row">
                                            {p.id}
                                        </TableCell>
                                        <TableCell>
                                            <Link to={`/products/${p.id}`}>
                                                {p.images.map(i => 
                                                (<img
                                                        src={import.meta.env.VITE_BACKEND_URL + "/files/" + i}
                                                        key={i}
                                                        style={{width: "24px", height: "24px", objectFit: "contain"}}
                                                        ></img>))}
                                            </Link>
                                        </TableCell>
                                        <TableCell>
                                            <Link to={`/products/${p.id}`}>
                                                {p.nom}
                                            </Link>
                                        </TableCell>
                                        <TableCell>{p.description}</TableCell>
                                        <TableCell align="right">{p.prix}</TableCell>
                                        <TableCell align="right">{p.stock}</TableCell>
                                        <TableCell align="right">
                                            <Link to={`/products/${p.id}/edit`}>
                                                <IconButton aria-label="edit product">
                                                    <EditIcon />
                                                </IconButton>
                                            </Link>
                                            <IconButton onClick={(evt) => onDelete(evt, p.id)} aria-label="delete product">
                                                <DeleteIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>

                    {products.length < count ? <Button variant="contained" onClick={onLoadMore}>More products ({products.length} / {count})</Button> : ''}
                    { authenticatedUser && authenticatedUser.role === "VENDEUR" ?
                        <Fab color="primary" aria-label="add" sx={{position: 'fixed', bottom: 20, right: 20}} onClick={onAdd}>
                            <AddIcon />
                        </Fab> :
                        ""
                    }

                </>
            )}
        </>
    )
}

export default ProductList