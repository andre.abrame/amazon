import axios from "axios";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom"
import CircularProgress from '@mui/material/CircularProgress';

function ProductDetails() {
    const { id } = useParams();
    // const [state, setState] = useState("LOADING")
    const [product, setProduct] = useState(undefined);

    useEffect(() => {
        axios.get(import.meta.env.VITE_BACKEND_URL + "/produits/" + id).then((response) => {
            setProduct(response.data);
          });
    }, [id]);




  return (
    <>
        { !product ? (
            <p><CircularProgress /> loading ...</p>
        ) : (
            <>
                <h1>Product {id}: {product.nom}</h1>
                <Link to="/products">back</Link>
                <div>{product.description} {product.prix}</div>
            </>
        )}
    </>
  )
}

export default ProductDetails