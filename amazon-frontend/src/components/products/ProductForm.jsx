import { Box, Button, TextField } from "@mui/material"
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { useFormValidation } from "../../shared/hooks/useFormValidation";
import PropTypes from 'prop-types';
import ImagePicker from "../../shared/components/ImagePicker";

function ProductForm({ title, editedProduct, onSubmit }) {
  const [ product, setProduct] = useState( editedProduct ?? { nom: "", description: "", prix: 1, stock: 0, images: []});
  const [ validate, isTouched, isValid, getMessages, markAsTouched ] = useFormValidation({
    nom: [
      n => n === "" ? "Required. " : "",
      n => n.length < 3 || n.length > 255 ? "Length must be between 3 and 255 characters. " : ""
    ],
    description: [],
    prix: [
      p => p < 0 ? "Must be grater or equal to 0. " : ""
    ],
    stock: []
  });
  const [ files, setFiles ] = useState(undefined);



  function handleChange(event) {
    // retrieve the name and value of the changed input
    const { name, value} = event.target;
    const newProduct = { ...product, [name]: value};
    // change state
    setProduct(newProduct);
    // apply validation rules
    validate(newProduct, name);
  }

  function handleSubmit(event) {
    // disable default submit
    event.preventDefault();
    // check validation
    // send data to backend
    onSubmit(product, files);
  }

  function handleBlur(event) {
    // retrieve the name blurred input
    const { name } = event.target;
    // change its touched status
    markAsTouched(name);
  }

  useEffect(() => {
    validate(product);
    return () => files?.forEach(f => window.URL.revokeObjectURL(f.src))
 }, []);

 useEffect(() => {
    if (editedProduct) {
        setProduct(editedProduct);
        validate(editedProduct);
    }
}, [editedProduct]);

  return (
    <>
      <h1>{title}</h1>
      <form id="new-product-form" onSubmit={handleSubmit}>
        <TextField onBlur={handleBlur} error={isTouched("nom") && !isValid("nom")} helperText={getMessages("nom") + " "} label="Name" name="nom" value={product.nom} onChange={handleChange}/>
        <TextField onBlur={handleBlur} error={isTouched("description") && !isValid("description")} helperText={getMessages("description") + " "} multiline rows={3} label="Description" name="description" value={product.description} onChange={handleChange}/>
        <TextField onBlur={handleBlur} error={isTouched("prix") && !isValid("prix")} helperText={getMessages("prix") + " "} type="number" label="Price" name="prix" value={product.prix} onChange={handleChange}/>
        <TextField onBlur={handleBlur} error={isTouched("stock") && !isValid("stock")} helperText={getMessages("stock") + " "} type="number" label="Stock" name="stock" value={product.stock} onChange={handleChange}/>
        <ImagePicker newFiles={files} setNewFiles={setFiles} existingFiles={product.images} setExistingFiles={(images) => setProduct({ ...product, images: images})}></ImagePicker>
        <Box sx={{display: "flex", flexDirection: "row", justifyContent: "space-around", margin: "0 auto", maxWidth: "500px"}}>
          <Link to="/products"><Button type="button" variant="outlined">Cancel</Button></Link>
          <Button type="submit" variant="contained" disabled={!isValid()}>Submit</Button>
        </Box>
      </form>
    </>
  )
}

ProductForm.propTypes = {
    title: PropTypes.string.isRequired,
    editedProduct: PropTypes.object,
    onSubmit: PropTypes.func.isRequired
};

export default ProductForm;