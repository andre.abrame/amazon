import React from 'react'
import PropTypes from 'prop-types'
import { Button, Card, CardActions, CardContent, CardMedia, Typography } from '@mui/material'

function ProductCard({ product }) {
    return (
        <Card sx={{ maxWidth: 345 }} key={product.id}>
            <CardMedia
                sx={{ height: 140 }}
                image={import.meta.env.VITE_BACKEND_URL + "/files/" + product.images[0]}
                title="green iguana"
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {product.nom}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {product.description}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Share</Button>
                <Button size="small">Learn More</Button>
            </CardActions>
        </Card>
    )
}

ProductCard.propTypes = {
    product: PropTypes.object
}

export default ProductCard
