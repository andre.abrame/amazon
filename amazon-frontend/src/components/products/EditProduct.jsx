import axios from "axios";
import ProductForm from "./ProductForm";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Alert, Snackbar } from "@mui/material";

function EditProduct() {
    const navigate = useNavigate();
    const [open, setOpen] = useState(false);
    const [message, setMessage] = useState("");
    const [severity, setSeverity] = useState("info");

    const { id } = useParams();
    const [product, setProduct] = useState(undefined);

    useEffect(() => {
        axios.get(import.meta.env.VITE_BACKEND_URL + "/produits/" + id).then((response) => {
            setProduct(response.data);
          });
    }, [id]);

  function handleSubmit(product, files) {
    // displaying snackbar
    setMessage("update in progresss")
    setSeverity("info")
    setOpen(true);
    // create data to send
    let formData = new FormData();
    for (const f of files)
      formData.append("newImages", f.file, f.file.name);
    Object.keys(product).forEach(k => formData.append(k, product[k]));
    formData.set("images", JSON.stringify(product.images));
    // send data to backend
    axios.put(`${import.meta.env.VITE_BACKEND_URL}/produits/${id}`, formData)
    .then(() => {
      // updating snackbar  
      setMessage("update succeed");
      setSeverity("success");
      setTimeout(() => {
        setOpen(false);
        navigate("/products");
      }, 2500);
    })
    .catch((error) => {
      // updating snackbar
      setMessage("update failed : " + error);
      setSeverity("error");
      setTimeout(() => {
        setOpen(false);
      }, 2500);
    })
  }

  return (
    <>
      <ProductForm title="Edit product" onSubmit={handleSubmit} editedProduct={product}></ProductForm>
      <Snackbar open={open}>
        <Alert severity={severity} sx={{ width: '100%' }}>
          {message}
        </Alert>
      </Snackbar>
      </>
  )
}

export default EditProduct