import axios from "axios";
import ProductForm from "./ProductForm";
import { useNavigate } from "react-router-dom";
import { Alert, Snackbar } from "@mui/material";
import React from "react";

function NewProduct() {

  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState("");
  const [severity, setSeverity] = React.useState("info");

  function handleSubmit(product, files) {
    // displaying snackbar
    setMessage("save in progresss")
    setSeverity("info")
    setOpen(true);
    // create data to send
    let formData = new FormData();
    for (const f of files)
      formData.append("newImages", f.file, f.file.name);
    Object.keys(product).forEach(k => formData.append(k, product[k]));
    // send data to backend
    axios.post(import.meta.env.VITE_BACKEND_URL + "/produits", formData)
      .then(() => {
        // updating snackbar  
        setMessage("save succeed");
        setSeverity("success");
        setTimeout(() => {
          setOpen(false);
          navigate("/products");
        }, 2500);
      })
      .catch((error) => {
        // updating snackbar
        setMessage("save failed : " + error);
        setSeverity("error");
        setTimeout(() => {
          setOpen(false);
        }, 2500);
      })
  }

  return (
    <>
      <ProductForm title="New product" onSubmit={handleSubmit}></ProductForm>
      <Snackbar open={open}>
        <Alert severity={severity} sx={{ width: '100%' }}>
          {message}
        </Alert>
      </Snackbar>
    </>
  )
}

export default NewProduct