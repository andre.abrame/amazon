import { AppBar, Avatar, Box, Button, IconButton, Menu, MenuItem, Toolbar, Tooltip, Typography } from '@mui/material'
import { useContext, useState } from 'react'
import { AuthenticationContext } from '../App'
import { Link } from 'react-router-dom';
import IsAnonymous from './security/conditions/IsAnonymous';
import IsAuthenticated from './security/conditions/IsAuthenticated';

function NavBar() {
  const [anchorElUser, setAnchorElUser] = useState(null);

  const [authenticatedUser, , logout] = useContext(AuthenticationContext);

  const handleLogout = () => {
    logout();
  }

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };


  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };


  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed">
        <Toolbar>
          <Link to={"/products"}><Button color="primary" type="button" variant="contained">Products</Button></Link>
          <Typography sx={{ flexGrow: 1 }}></Typography>
            <IsAuthenticated>
              <Button color="primary" type="button" variant="contained" onClick={handleLogout}>Logout</Button>
              Hello {authenticatedUser?.name} !
            </IsAuthenticated>
            <IsAnonymous>
              <Link to="/login"><Button color="primary" type="button" variant="contained">Login</Button></Link>
              <Link to="/register"><Button color="primary" type="button" variant="contained">Register</Button></Link>
            </IsAnonymous>
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default NavBar