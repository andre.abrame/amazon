import { useContext } from 'react'
import { AuthenticationContext } from '../../../App';
import PropTypes from 'prop-types';

function IsAnonymous({children}) {

    const [authenticatedUser, , , ] = useContext(AuthenticationContext);
    
    return (
    <>
        {(!authenticatedUser) ? children : ''}
    </>);
}

IsAnonymous.propTypes = {
    children: PropTypes.any
};

export default IsAnonymous