import { useContext } from 'react'
import { AuthenticationContext } from '../../../App';
import PropTypes from 'prop-types';

function IsAuthenticated({children}) {

    const [authenticatedUser, , , ] = useContext(AuthenticationContext);
    
    return (
    <>
        {(authenticatedUser) ? children : ''}
    </>);
}

IsAuthenticated.propTypes = {
    children: PropTypes.any
};

export default IsAuthenticated