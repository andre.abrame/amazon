import { useContext } from 'react'
import { AuthenticationContext } from '../../../App';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';

function AnonymousGuard({children}) {

    const [authenticatedUser, , , ] = useContext(AuthenticationContext);
    
    return (
    <>
        {(!authenticatedUser) ? children : <Navigate to={"/"} replace />}
    </>);
}

AnonymousGuard.propTypes = {
    children: PropTypes.any
};

export default AnonymousGuard