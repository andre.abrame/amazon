import { useEffect, useState } from 'react'
import axios from 'axios';
import ProductCard from './products/ProductCard';



function Home() {
  const [products, setProducts] = useState(undefined);
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log(import.meta.env);
    axios.get(import.meta.env.VITE_BACKEND_URL + "/produits?page=0&pageSize=10").then((response) => {
      response.data.results.forEach(p => p.createdAt = new Date(p.createdAt));
      setProducts(response.data.results);
      setCount(response.data.count);
    });
  }, []);

  return (
    <>
      {products?.map(p => (
        <ProductCard key={p.id} product={p}></ProductCard>
      ))}
    </>
  )
}

export default Home
