import { useContext } from "react";
import { AuthenticationContext } from "../App";

function NotImplemented() {
    const [authenticatedUser, , ] = useContext(AuthenticationContext);
    return (
        <>
            <h1>Not implemented :(</h1>
            {JSON.stringify(authenticatedUser)}
        </>
    )
}

export default NotImplemented;