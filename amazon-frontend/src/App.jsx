import { Route, Routes } from 'react-router-dom'
import ProductList from './components/products/ProductList'
import ProductDetails from './components/products/ProductDetails'
import NewProduct from './components/products/NewProduct'
import EditProduct from './components/products/EditProduct'
import NavBar from './components/NavBar'
import { createContext, useEffect, useState } from 'react'
import axios from 'axios'
import jwt_decode from "jwt-decode";
import Login from './components/Login'
import AuthenticationInterceptor from './interceptors/authentication.interceptor'
import AnonymousGuard from './components/security/guards/AnonymousGuard'
import Register from './components/Register'
import Activate from './components/Activate'
import { connect } from 'socket.io-client';
import { disconnect, socket } from './shared/sockets'
import Home from './components/Home'
import logo from "./assets/react.svg";

export const AuthenticationContext = createContext()

function App() {

  const [authenticatedUser, setAuthenticatedUser] = useState(undefined);

  const login = (email, password) => {
    return axios.post(`${import.meta.env.VITE_BACKEND_URL}/authenticate`, {
      grantType: "password",
      username: email,
      password: password
    }).then((response) => {
      const { accessToken, refreshToken } = response.data;
      const decodedAccessToken = jwt_decode(accessToken);
      setAuthenticatedUser({
        id: decodedAccessToken.sub,
        name: decodedAccessToken.name,
        role: decodedAccessToken.role,
        accessToken: accessToken,
        refreshToken: refreshToken
      });
    });
  }

  const logout = () => {
    setAuthenticatedUser(undefined);
  }

  const refresh = () => {
    console.log("REFRESH " + JSON.stringify(authenticatedUser?.refreshToken))
    return axios
      .post(`${import.meta.env.VITE_BACKEND_URL}/authenticate`, {
        grantType: "refreshToken",
        refreshToken: authenticatedUser?.refreshToken
      })
      .then(res => {
        if (res.status === 200) {
          setAuthenticatedUser({
            ...authenticatedUser,
            accessToken: res.data.accessToken,
            refreshToken: res.data.refreshToken
          })
          return res.data.accessToken;
        }
      })
      .catch(err => {
        setAuthenticatedUser(undefined);
        return err;
      })
  }

  useEffect(() => {
    if (localStorage.getItem("authenticatedUser"))
      setAuthenticatedUser(JSON.parse(localStorage.getItem("authenticatedUser")));
  }, []);

  useEffect(() => {
    if (authenticatedUser && authenticatedUser.role === "VENDEUR" && !socket) {
      connect();
    } else if (socket) {
      disconnect();
    }
}, [authenticatedUser]);

  useEffect(() => {
    if (authenticatedUser)
      localStorage.setItem("authenticatedUser", JSON.stringify(authenticatedUser));
    else
      localStorage.removeItem("authenticatedUser");
  }, [authenticatedUser]);

  return (
    <>
      <AuthenticationContext.Provider value={[authenticatedUser, login, logout, refresh]}>
        <AuthenticationInterceptor>
          <NavBar></NavBar>
          <div className="content">
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/products" element={<ProductList />} />
              <Route path="/products/new" element={<NewProduct />} />
              <Route path="/products/:id" element={<ProductDetails />} />
              <Route path="/products/:id/edit" element={<EditProduct />} />
              <Route path="/login" element={<AnonymousGuard><Login /></AnonymousGuard>} />
              <Route path="/register" element={<AnonymousGuard><Register /></AnonymousGuard>} />
              <Route path="/activate/:token" element={<AnonymousGuard><Activate /></AnonymousGuard>} />
            </Routes>
          </div>
          <img  src={logo}/>
        </AuthenticationInterceptor>
      </AuthenticationContext.Provider>
    </>
  )
}

export default App
