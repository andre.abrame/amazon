import { useState } from "react";


export function useFormValidation(validators) {
    const fields = Object.keys(validators).reduce((m, k) => {
        const [messages, setMessages] = useState("");
        const [touched, setTouched] = useState(false);    
        return { ...m, [k]: {
            messages : messages,
            setMessages: setMessages,
            touched: touched,
            setTouched: setTouched
        }};
    }, {});

    const validate = (object, field) => {
        const fieldsToValidate = field ? [field] : Object.keys(fields);
        for (let field of fieldsToValidate) {
            fields[field]?.setMessages(validators[field]
                .map(v => v(object[field]))
                .join(""));
        }
    }

    const isTouched = (field) => {
        return fields[field]?.touched;
    }

    const isValid = (field) => {
        if (field)
            return !fields[field] || fields[field].messages === "";
        return Object.keys(fields)
            .map(key => fields[key].messages)
            .every(value => value === "");
    }

    const getMessages = (field) => {
        if (isTouched(field))
            return fields[field]?.messages;
        return "";
    }
    
    const markAsTouched = (key) => {
        if (fields[key])
            fields[key].setTouched(true);
    }

    return [validate, isTouched, isValid, getMessages, markAsTouched]
}

