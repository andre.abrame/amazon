import { io } from "socket.io-client";


export let socket;

export function connect() {
    socket = io(import.meta.env.VITE_BACKEND_URL + "");
}

export function disconnect() {
    socket.disconnect();
    socket = undefined;
}