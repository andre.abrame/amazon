import { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { Box, IconButton } from '@mui/material'
import AddIcon from '@mui/icons-material/Add';
import ClearIcon from '@mui/icons-material/Clear';
import './ImagePicker.css'

function ImagePicker({ existingFiles, setExistingFiles, newFiles, setNewFiles }) {

    const inputRef = useRef();

    function dropHandler(event) {
        event.preventDefault();
        onAddFiles(event.dataTransfer.files);
    }

    function dragOverHandler(event) {
        event.preventDefault();
        event.dataTransfer.dropEffect = "move";
    }

    function inputChangeHandler(event) {
        console.log("chnged")
        event.preventDefault();
        onAddFiles(event.target.files);

    }

    function onAddClick() {
        inputRef.current.click();
    }

    function onAddFiles(files) {
        setNewFiles([
            ...(newFiles ?? []),
            ...Array.from(files || []).map(file => ({
                file: file,
                src: window.URL.createObjectURL(file)
            }))
        ]);
    }

    function onRemoveNew(file) {
        const index = newFiles.findIndex(f => f === file);
        if (index != -1)
            setNewFiles([
                ...newFiles.slice(0, index),
                ...newFiles.slice(index + 1)
            ]);
        window.URL.revokeObjectURL(file.src);
    }

    function onRemoveExisting(fileSrc) {
        const index = existingFiles.findIndex(f => f === fileSrc);
        if (index != -1)
            setExistingFiles([
                ...existingFiles.slice(0, index),
                ...existingFiles.slice(index + 1)
            ]);
    }

    useEffect( () => () => newFiles?.forEach(f => window.URL.revokeObjectURL(f.src)), [] );

    return (
        <>
            <Box
                className="image-picker"
                onDragOver={dragOverHandler}
                onDrop={dropHandler}
            >

                {existingFiles?.map((fileSrc) => (
                    <Box key={fileSrc} className="image">
                        <img
                            src={import.meta.env.VITE_BACKEND_URL + "/files/" + fileSrc}
                            style={{}}
                        ></img>
                        <Box>
                            <IconButton
                                aria-label="delete"
                                size="large"
                                color="error"
                                onClick={() => onRemoveExisting(fileSrc)}
                            ><ClearIcon fontSize='inherit' /></IconButton>
                        </Box>
                    </Box>
                ))}

                {newFiles?.map((file) => (
                    <Box key={file.src} className="image">
                        <img
                            src={file.src}
                            style={{}}
                        ></img>
                        <Box>
                            <IconButton
                                aria-label="delete"
                                size="large"
                                color="error"
                                onClick={() => onRemoveNew(file)}
                            ><ClearIcon fontSize='inherit' /></IconButton>
                        </Box>
                    </Box>
                ))}
                <IconButton
                className="add"
                    aria-label="add"
                    size="large"
                    color="primary"
                    onClick={onAddClick}
                ><AddIcon fontSize='inherit' /></IconButton>
            </Box>
            <input ref={inputRef} type="file" multiple onChange={inputChangeHandler} hidden/>
        </>
    )
}

ImagePicker.propTypes = {
    existingFiles: PropTypes.array,
    setExistingFiles: PropTypes.func,
    newFiles: PropTypes.array,
    setNewFiles: PropTypes.func
}

export default ImagePicker
