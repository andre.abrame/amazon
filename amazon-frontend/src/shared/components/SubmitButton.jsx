import PropTypes from 'prop-types'
import { Button } from '@mui/material'

function SubmitButton({ status, ...rest }) {
    return (
        <Button variant="outlined" startIcon={status.icon} color={status.color} {...rest}>
            {status.content}
        </Button>
    )
}

SubmitButton.propTypes = {
    status: PropTypes.object
}

export default SubmitButton
