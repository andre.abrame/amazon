import { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { Box, IconButton } from '@mui/material'
import AddIcon from '@mui/icons-material/Add';
import ClearIcon from '@mui/icons-material/Clear';
import './ImagePicker.css'

function SingleImagePicker({ existingFile, setExistingFile, newFile, setNewFile }) {

    const inputRef = useRef();

    function dropHandler(event) {
        event.preventDefault();
        onReplaceFile(event.dataTransfer.files[0]);
    }

    function dragOverHandler(event) {
        event.preventDefault();
        event.dataTransfer.dropEffect = "move";
    }

    function inputChangeHandler(event) {
        event.preventDefault();
        onReplaceFile(event.target.files[0]);

    }

    function onAddClick() {
        inputRef.current.click();
    }

    function onReplaceFile(file) {
        setNewFile({
            file: file,
            src: window.URL.createObjectURL(file)
        });
        if (existingFile)
            setExistingFile(undefined);
    }

    function onRemoveFile() {
        if (newFile) {
            window.URL.revokeObjectURL(newFile.src);
            setNewFile(undefined);
        }
        if (existingFile)
            setExistingFile(undefined);
    }

    useEffect(
        () => () => newFile && window.URL.revokeObjectURL(newFile.src), 
        []
    );

    return (
        <>
            <Box
                className="image-picker"
                onDragOver={dragOverHandler}
                onDrop={dropHandler}
            >

                {existingFile && (
                    <Box className="image">
                        <img
                            src={import.meta.env.VITE_BACKEND_URL + "/files/" + existingFile}
                            style={{}}
                        ></img>
                        <Box>
                            <IconButton
                                aria-label="delete"
                                size="large"
                                color="error"
                                onClick={() => onRemoveFile()}
                            ><ClearIcon fontSize='inherit' /></IconButton>
                        </Box>
                    </Box>
                )}


                {newFile && (
                    <Box className="image">
                        <img
                            src={newFile.src}
                            style={{}}
                        ></img>
                        <Box>
                            <IconButton
                                aria-label="delete"
                                size="large"
                                color="error"
                                onClick={() => onRemoveFile()}
                            ><ClearIcon fontSize='inherit' /></IconButton>
                        </Box>
                    </Box>
                )}

                {(!existingFile && !newFile && (
                    <IconButton
                    className="add"
                        aria-label="add"
                        size="large"
                        color="primary"
                        onClick={onAddClick}
                    ><AddIcon fontSize='inherit' /></IconButton>
                ))}
            </Box>
            <input ref={inputRef} type="file" onChange={inputChangeHandler} hidden/>
        </>
    )
}

SingleImagePicker.propTypes = {
    existingFile: PropTypes.string,
    setExistingFile: PropTypes.func,
    newFile: PropTypes.object,
    setNewFile: PropTypes.func
}

export default SingleImagePicker
