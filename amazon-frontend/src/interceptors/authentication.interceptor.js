import axios from 'axios'
import { useNavigate } from 'react-router-dom';
import { AuthenticationContext } from '../App';
import { useContext, useEffect } from 'react';
import jwt_decode from "jwt-decode";

let authorizationInterceptor;
let refreshTokenInterceptor;

function AuthenticationInterceptor({ children }) {

    const navigate = useNavigate();
    const [authenticatedUser, , , refresh] = useContext(AuthenticationContext)

    // called each time authenticatedUser changes
    useEffect(() => {
        console.log("UPDATE INTERCEPTOR");
        // remove old interceptor
        if (authorizationInterceptor)
            axios.interceptors.request.eject(authorizationInterceptor);
        // add a new one, with updated authenticatedUser value
        authorizationInterceptor = axios.interceptors.request.use(
            config => {
                console.log("INTERCEPT");
                const token = localStorage.getItem('authenticatedUser')?.accessToken;
                if (token &&
                    config.url !== import.meta.env.VITE_BACKEND_URL + "/authenticate" &&
                    !config.headers['Authorization']) {
                    config.headers['Authorization'] = 'Bearer ' + token
                }
                return config
            },
            error => {
                Promise.reject(error)
            }
        );
        // remove old interceptor
        if (refreshTokenInterceptor)
            axios.interceptors.request.eject(refreshTokenInterceptor);
        // add a new one, with updated authenticatedUser value
        refreshTokenInterceptor = axios.interceptors.response.use(
            response => {
                return response
            },
            async function (error) {
                const originalRequest = error.config

                // if (error.response.status === 401 &&
                //     (!authenticatedUser ||
                //         originalRequest.url.endsWith("authenticate") ||
                //         jwt_decode(authenticatedUser.refreshToken).exp > Date.now())) {
                //     navigate('/login')
                //     return Promise.reject(error)
                // }
                // else
                if (error.response.status === 401 &&
                    !originalRequest._retry &&
                    !originalRequest.url.endsWith("authenticate")) {
                    originalRequest._retry = true
                    const accessToken = await refresh();
                    originalRequest.headers['Authorization'] = 'Bearer ' + accessToken;
                    return axios(originalRequest)
                }
                //  else
                //     return Promise.reject(error)
            }
        );
    }, [authenticatedUser, navigate, refresh]);

    return children
}

export default AuthenticationInterceptor
