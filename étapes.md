

# backend : mise en place du projet

1. faire les diagrammes de classe et de cas d'utilisation
2. créer un répertoire avec 2 sous-dossiers, un pour le back et l'autre pour le front
3. dans le dossier du back :
    - initialiser avec npm : `npm init`
    - installer les dépendances : `npm install dotenv`
    - créer un fichier `index.js` et un répertoire `src`
    - créer un fichier `.env`
    - ajouter dans `package.json` : `'type': 'module'`
4. git :
    - créer un dépot vide sur `gitlab` ou `github`
    - initialise un dépot local (dans le répertoire parent) :
        - `git init`
        - `git remote add origin <url du depot>`
        - créer un fichier `.gitignore` sur le site <https://www.toptal.com/developers/gitignore> et le copier dans le dossier back
        - `git add .`
        - `git commit -m "un super message"`
        - `git push -u origin main`


# backend : création de la couche de persistance

1. installer les dépendances : `npm install sequelize mysql2`
2. ajouter au `.env` l'url de la base de donnée
3. créer un fichier `db.config.js` dans un répertoire `configs` dans lequel on initialise la connexion à la base de données
4. créer les models dans le répertoire `models`
5. dans le `index.js` faire la synchronisation de la base de données

# backend : couche de présentation

1. installer les dépendances : `npm install express`
2. dans un dossier `controllers`, pour chaque entité métier ajouter les méthodes permettant de faire du crud :
    - `findAll`
    - `findById`
    - `save`
    - `update`
    - `delete`
3. dans un dossier `routes`, pour chaque entité métier lier les méthodes des controlleurs aux urls/méthodes HTTP
4. charger les routes dans `index.js`
5. tester avec postman

# frontend : mise en place du projet

1. créer un projet avec vite : `npm create vite`, puis répondre aux question posées
2. placer le terminale dans le répertoire du front : `cd <repertoire-du-front>`
3. installer les dépendances : `npm install axios react-router-dom`
4. ajouter `<BrowserRouter>` autour de `<App>` dans `main.jsx`
5. définir des routes dans `App.jsx` :

    ```js
    <Routes>
        <Route path="/" element={<NotImplemented/>}/>
        <Route path="/products" element={<ProductList/>}/>
        <Route path="/products/new" element={<NotImplemented/>}/>
        <Route path="/products/:id" element={<ProductDetails/>}/>
        <Route path="/products/:id/edit" element={<NotImplemented/>}/>
    </Routes>
    ```
6. Faire des composants et les associés aux routes

# frontend : vue d'une liste de ressource

1. Créer un composant `Products.jsx` avec :
    - un state pour la liste des produits :

        ```js
        const [products, setProducts] = useState(undefined);
        ```
    - un effect pour récupérer les produits auprès du back :

        ```js
        useEffect(() => {
            axios.get("http://localhost:3000/produits").then((response) => {
                setProducts(response.data);
            });
        }, []);
        ```
    - dans le jsx, afficher les produits :

        ```js
        return (
        <>
            <h1>ProductList</h1>
            {!products ? (
                <p><CircularProgress /> loading ...</p>
            ) : (
                <ul>
                    {products.map(p => (
                        <li>{p.nom}</li>
                    ))}
                </ul>
            )}
        </>
    )
        ```

# frontend : formulaire de création et modification d'une ressource

1. Créer un composant `<nom de la ressource>Form.jsx` et :
    - ajouter des props : `title`, `editedTessource`, et `onSubmit` :

        ```js
        function ProductForm({ title, editedProduct, onSubmit }) {
        ```
        Ne pas oublier la définition des props avec `PropType` :

        ```js
        ProductForm.propTypes = {
            title: PropTypes.string.isRequired,
            editedProduct: PropTypes.object,
            onSubmit: PropTypes.func.isRequired
        };
        ```
    - ajouter une state avec une ressoruce vide (un objet avec des valeurs par défaut) :

        ```js
        const [ product, setProduct] = useState( editedProduct ?? { nom: "", description: "", prix: -1, stock: 0});
        ```
    - créer le jsx avec les champs de formulaire. Pour chaque `<input>` lier l'attribut `value` au state  :

        ```js
        return (
            <>
            <h1>{title}</h1>
            <form id="new-product-form">
                <TextField label="Name" name="nom" value={product.nom} variant="filled" />
                <TextField multiline rows={3} label="Description" name="description" value={product.description} variant="filled" />
                <TextField type="number" label="Price" name="prix" value={product.prix} variant="filled" />
                <TextField type="number" label="Stock" name="stock" value={product.stock} variant="filled" />
                <div>
                <Link to="/products"><Button type="button" variant="outlined">Cancel</Button></Link>
                <Button type="submit" variant="contained">Submit</Button>
                </div>
            </form>
            </>
        )
        ```
    - ajouter la fonction `handleChange` qui extrait le n om et la valeur du `input` modifié et qui met à jour le state :

        ```js
        function handleChange(event) {
            // retrieve the name and value of the changed input
            const { name, value} = event.target;
            const newProduct = { ...product, [name]: value};
            // change state
            setProduct(newProduct);
        }
        ```
    - Pour chaque `<input>`, lier l'event `change` à la fonction précédente :

        ```js
        <TextField label="Name" name="nom" value={product.nom} variant="filled" onChange={handleChange}/>
        ```
    - ajouter la fonction `handleSubmit` qui appelle la props `onSubmit` :
    
        ```js
        function handleSubmit(event) {
            event.preventDefault();
            onSubmit(product);
        }
        ```
        La lier à l'evennement `submit` du formulaire :

        ```js
        <form id="new-product-form" onSubmit={handleSubmit}>
        ```
    - validation :
        - ajouter le hooks personnalisé `useFormValidation` (copier/coller depuis amazon)
        - créer le hooks dans le composant
        - appeller la méthode `validate` au chargement (dans un `useEffect`) et lors des changements (dans `handleChange`)
        - créer `onBlur` pour mettre à jour le status *touched* lié à la validation
        - dans le jsx, utiliser `isTouched`, `isValid` et `getMessage` pour afficher les erreurs de validation
2. Créer un composant `New<nom de la ressource>.jsx` et :
    - créer une méthode `handleSubmit` faisant une requète POST au backend :

        ```js
        function handleSubmit(product) {
            // send data to backend
            axios.post("http://localhost:3000/produits", product);
        }
        ```
    - dans le jsx, appeler le composant `<nom de la ressource>Form` en lui passant les props `title` et `onSubmit` :

        ```js
        return (
            <ProductForm title="New product" onSubmit={handleSubmit}></ProductForm>
        )
        ```
3. Créer un composant `Edit<nom de la ressource>.jsx` et :
    - extraire l'id de la ressource à éditer de l'url : `const { id } = useParams();`
    - dans un `useEffect`, faire un appel au backend pour récupérer la ressource à éditer :

        ```js
        useEffect(() => {
            axios.get("http://localhost:3000/produits/" + id).then((response) => {
                setProduct(response.data);
            });
        }, [id]);
        ```
    - créer une méthode `handleSubmit` faisant une requète PUT au backend :

        ```js
        function handleSubmit(product) {
            // send data to backend
            axios.put(`http://localhost:3000/produits/${id}`, product);
        }
        ```
    - dans le jsx, appeler le composant `<nom de la ressource>Form` en lui passant les props `title`, la ressource à éditer et `onSubmit` :

        ```js
        return (
            <ProductForm title="Edit product" onSubmit={handleSubmit} editedProduct={product}></ProductForm>
        )
        ```

# backend : securisation

1. ajouter la librairie `jsonwebtoken` :

    ```sh
    npm install jsonwebtoken
    ```
2. générer un secret :
    - lancer node puis écrire : `require('crypto').randomBytes(64).toString('hex')`
    - ajouter la valeur générée dans le .env du backend, avec la clé `jwt.secret`
3. créer un controller `authentication.controller.js`
    - en faisant un copier/coller d'amazon
    - puis le modifier selon les tables contenant les users
4. créer un middleware extrayant et validant les tokens des requètes : copier/coller d'amazon
5 appliquer ce middleware à toutes les requètes :

    ```js
    app.use(jwtMiddleware);
    ```
6. créer des middlewares vérifiant les droit d'accès aux controleurs
7. appliquer ces middlewares sur les routes des controleurs


# frontend : securisation

1. Créer un contexte dans le `App.jsx` contenant :
    - un state avec le status de connexion (id, role, et les tokens)
    - des méthodes `login`, `logout` et `refresh`
    - un effect lisant dans le `localStorage` au démarrage et actualisant le status
    - un effect actualisant le `localStorage` à chaque changement de status
2. Créer un composant login utilisant les méthodes du context lors de la soumission
3. Ajouter un bouton logout utilisant les méthodes du context
4. Masquer les éléments d'affichage selon l'utilisateur connecté
5. Ajouter les interceptors pour
    - ajouter le headder `Authorization` dans les requètes
    - rafraichir le access token s'il est expiré

# upload de fichier

## frontend

- dans votre formulaire, ajouter un `input` de type file (ou utiliser `ImagePicker`)
- lors de la soumission :
    - créer un objet `FormData`

        ```js
        let formData = new FormData();
        ```
    - ajouter chaque image à l'objet (`append`)

        ```js
        for (const f of files)
            formData.append("images", f.file, f.file.name);
        ```
    - ajouter les autres attributs de l'objet à sauvegarder (toujours `append`)

        ```js
        Object.keys(product).forEach(k => formData.append(k, product[k]));
        ```
    - envoyer l'objet `FormData` avec axios :

        ```js
        axios.post("http://localhost:3000/produits", formData).then(...)
        ```

## backend

- ajouter la librairie [multer](https://github.com/expressjs/multer) :

    ```sh
    npm install multer 
    ```
- ajouter au `.env` une proriété `files.path` ayant pour valeur le répertoire de destination des fichiers uploadés
- ajouter un fichier de config `uploader.config.js` :

    ```js
    import multer from 'multer'
    const uploader = multer({ dest: process.env["files.path"] });
    export default uploader
    ```
- ajouter le middleware sur les controlleur :

    ```js
    router.post("/", uploader.array("images"), ProduitController.save);
    ```
- dans le controller :
    - mettre à jour l'objet à sauvegarder avec les images

    ```js
    produit.images = req.files.map(f => f.filename);
    ```
    - supprimer les images en cas d'erreur

    ```js
    } catch (error) {
        req.files.forEach(f => unlink(f.path, (err) => {
            if (err) throw err;
        }));
    ```
- dans le `index.js`, dire à express de rendre accessible les fichiers uploadés :

    ```js
    app.use('/files', express.static(process.env["files.path"]));
    ```