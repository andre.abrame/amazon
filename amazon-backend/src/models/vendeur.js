import { DataTypes } from "sequelize";
import { sequelize } from "../configs/db.config.js";
import { Produit } from "./produit.js";

export const Vendeur = sequelize.define("Vendeur", {
 prenom: DataTypes.STRING,
 nom: DataTypes.STRING,
 email: DataTypes.STRING,
 password: DataTypes.STRING,
 siret: DataTypes.INTEGER,
 role: {
    type: DataTypes.ENUM('VENDEUR', 'CLIENT'),
    defaultValue: 'VENDEUR'
 },
 activated: {
  type: DataTypes.BOOLEAN,
  defaultValue: false
 },
 token: {
  type: DataTypes.STRING,
  unique: true
 }
});

Vendeur.hasMany(Produit);
Produit.belongsTo(Vendeur);