import { DataTypes } from "sequelize";
import { sequelize } from "../configs/db.config.js";
import { Client } from "./client.js";
import { Produit } from "./produit.js";

export const Note = sequelize.define("Note", {
    note: DataTypes.INTEGER
});

Client.hasMany(Note);
Note.belongsTo(Client);
Produit.hasMany(Note);
Note.belongsTo(Produit);