import { DataTypes } from "sequelize";
import { sequelize } from "../configs/db.config.js";

export const Produit = sequelize.define('Produit', {
    // Model attributes are defined here
    nom: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true,
            len: [3,255]
        },
    },
    description: DataTypes.STRING,
    prix: {
        type: DataTypes.DECIMAL,
        validate: {
            min: 0
        },
        defaultValue: 0,
        allowNull: false
    },
    stock: {
        type: DataTypes.INTEGER,
        validate: {
            min: 0
        },
        defaultValue: 0,
        allowNull: false
    },
    images: DataTypes.JSON
});