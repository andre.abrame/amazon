import { DataTypes } from "sequelize";
import { sequelize } from "../configs/db.config.js";

export const Client = sequelize.define("Client", {
 prenom: DataTypes.STRING,
 nom: DataTypes.STRING,
 email: {
   type: DataTypes.STRING,
   unique: true
 },
 password: DataTypes.STRING,
 role: {
    type: DataTypes.ENUM('VENDEUR', 'CLIENT'),
    defaultValue: 'CLIENT'
 },
 activated: {
  type: DataTypes.BOOLEAN,
  defaultValue: false
 },
 token: {
  type: DataTypes.STRING,
  unique: true
 }
});