import { DataTypes } from "sequelize";
import { sequelize } from "../configs/db.config.js";
import { Client } from "./client.js";
import { Produit } from "./produit.js";

export const LignePanier = sequelize.define("LignePanier", {
    quantite: DataTypes.INTEGER
});

Client.hasMany(LignePanier);
LignePanier.belongsTo(Client);
Produit.hasMany(LignePanier);
LignePanier.belongsTo(Produit);