
import express from 'express';
import * as VendeurController from '../controllers/vendeur.controller.js';
export const router = express.Router();

router.get("/", VendeurController.findAll);
router.get("/:id", VendeurController.findById);
router.post("/", VendeurController.save);
router.put("/:id", VendeurController.update);
router.delete("/:id", VendeurController.remove);
router.get("/:id/produits", VendeurController.findProduitsByVendeurId);