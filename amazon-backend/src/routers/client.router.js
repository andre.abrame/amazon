
import express from 'express';
import { deleteClient, findAllClients, findClientById, saveClient, updateClient } from '../controllers/client.controller.js';
export const router = express.Router();

router.get("/", findAllClients);
router.get("/:id", findClientById);
router.post("/", saveClient);
router.put("/:id", updateClient);
router.delete("/:id", deleteClient);