
import express from 'express';
import * as ProduitController from '../controllers/produit.controller.js';
import { hasRole, isVendeur } from '../middlewares/authorization.middleware.js';
import uploader from '../configs/uploader.config.js';
export const router = express.Router();


/**
 * @openapi
 * /:
 *   get:
 *     description: return the list of products
 *     responses:
 *       200:
 *         description: Returns a mysterious string.
 */
router.get("/", ProduitController.findAll);
router.get("/:id", ProduitController.findById);
router.post("/", hasRole('ADMIN'), uploader.array("newImages"), ProduitController.save);
router.put("/:id", uploader.array("newImages"), ProduitController.update);
router.delete("/:id", ProduitController.remove);