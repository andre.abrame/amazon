
import express from 'express';
import { authenticate, register, activate } from '../controllers/authentication.controller.js';
export const router = express.Router();

router.post("/authenticate", authenticate);
router.post("/register", register);
router.post("/activate/:token", activate);