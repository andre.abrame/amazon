export const isAnonymous = (req, res, next) => {
    if (req.authenticatedUser) {
        res.sendStatus(403);
    } else {
        next();
    }
}

export const isAuthenticated = (req, res, next) => {
    if (!req.authenticatedUser) {
        res.sendStatus(401);
    } else {
        next();
    }
}

export const isVendeur = (req, res, next) => {
    if (!req.authenticatedUser)
        res.sendStatus(401);
    else if (req.authenticatedUser.role !== 'VENDEUR') {
        res.sendStatus(403);
    } else {
        next();
    }
}

export const isClient = (req, res, next) => {
    if (!req.authenticatedUser)
        res.sendStatus(401);
    else if (req.authenticatedUser.role !== 'CLIENT') {
        res.sendStatus(403);
    } else {
        next();
    }
}


export const hasRole = (role) => {
    return (req, res, next) => {
        if (!req.authenticatedUser)
            res.sendStatus(401);
        else if (req.authenticatedUser.role !== role) {
            res.sendStatus(403);
        } else {
            next();
        }
    }
}


export const hasAnyRole = (roles) => {
    return (req, res, next) => {
        if (!req.authenticatedUser)
            res.sendStatus(401);
        else if (roles.every(role => req.authenticatedUser.role !== role)) {
            res.sendStatus(403);
        } else {
            next();
        }
    }
}