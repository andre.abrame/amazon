export const ValidationErrorHandler = (err, req, res, next) => {
    if (err instanceof ValidationError)
        res.status(400).json(err);
    else
        next(err);
}

export const TokenExpiredErrorHandler = (err, req, res, next) => {
    if (err instanceof TokenExpiredError)
      res.status(401).json(err);
    else
        next(err);
}