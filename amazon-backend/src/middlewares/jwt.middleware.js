import jwt from 'jsonwebtoken';

export const jwtMiddleware = (req, res, next) => {
    // extract the jwt token if any
    const authorizationHeader = req.get("Authorization");
    if (authorizationHeader) {
        if (!authorizationHeader.match(/^Bearer /)) {
            res.status(401).send({message: "authorization should be of bearer type"});
            return;
        }
        const jwtToken = authorizationHeader.slice(7);
        // validate the jwt token
        let decodedJwtToken;
        try {
            decodedJwtToken = jwt.verify(jwtToken, process.env['jwt.secret']);
        } catch (err) {
            res.status(401).send({message: "invalid token : " + JSON.stringify(err)});
            return;
        }
        // put token data in req
        req.authenticatedUser = decodedJwtToken;
    }
    next();
}