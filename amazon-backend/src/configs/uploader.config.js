import multer from 'multer'

const uploader = multer({ dest: process.env["files.path"] });

export default uploader