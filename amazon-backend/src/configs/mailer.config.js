import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
    host: "smtp.laposte.net",
    port: 465,
    secure: true,
    auth: {
      user: process.env["mail.username"],
      pass: process.env["mail.password"]
    },
  });

export default transporter;