import { Op } from "sequelize";
import { Produit } from "../models/produit.js";
import { Vendeur } from "../models/vendeur.js";
import { unlink } from 'node:fs';



export const findAll = async (req, res, next) => {
    try {
        const parameters = { where: {}, include: [ { model: Vendeur }] };
        // pagination : recuperation des parametres de requetes
        const page = req.query.page ? parseInt(req.query.page) : undefined;
        const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : undefined;
        // pagination : configuration de la requete
        if (page !== undefined && pageSize !== undefined) {
            parameters.limit = pageSize;
            parameters.offset = page*pageSize;
        }
        // filtrage : recuperation des parametres de requetes
        const nameQuery = req.query.nom;
        const stockQuery = req.query.stock;
        const vendeurNameQuery = req.query.nomVendeur;
        // filtrage : configuration de la requete
        if (nameQuery)
            parameters.where.nom = { [Op.substring]: nameQuery };
        if (stockQuery)
            parameters.where.stock = { [Op.gte]: stockQuery };
        if (vendeurNameQuery)
            parameters.include.push({
                model: Vendeur,
                where: {
                    nom: { [Op.substring]: vendeurNameQuery }
                }
            });
        // // tri : recuperation des parametres de requetes
        // const order = req.query.order;
        // // tri : configuration de la requete
        // if (order) {
        //     parameters.order = [[order.split(",")[0], order.split(",")[1]]];
        // }
        // tri : recuperation des parametres de requetes
        const [attribute, direction] = req.query.order ? req.query.order.split(",") : [undefined,undefined];
        // tri : configuration de la requete
        if (attribute && direction) {
            parameters.order = [[attribute, direction]];
        }
        console.log(parameters);
        // execution de la requete SQL (avec les options définies précedemment)
        const produits = await Produit.findAll(parameters);
        const count = await Produit.count({ ...parameters, limit: undefined, offset: undefined });
        // envoi de la réponse
        res.json({
            count: count,
            results: produits
        });
    } catch (error) {
        next(error);
    }
};

// // find all en SQL pur
// export const findAllSql = async (req, res) => {
//     // recuperation des parametres de requete
//     const nameQuery = req.query.nom;
//     const stockQuery = req.query.stock;
//     const vendeurNameQuery = req.query.nomVendeur;
//     // analyse des parametres de requete et construction des options du findAll
//     let statement = "SELECT * FROM Produit p";
//     let replacements = {};
//     // ajout des jointures
//     if (vendeurNameQuery)
//         statement += " JOIN Vendeur v ON p.VendeurId == v.id";
//     // ajout des where
//     if (nameQuery || stockQuery || vendeurNameQuery)
//         statement += " WHERE";
//         replacements.nameQuery
//     }
//     if (nameQuery) {
//         statement += " p.name LIKE %:nameQuery%"
//         replacements.nameQuery = nameQuery;
//     }
//     if (stockQuery) {
//         statement += (nameQuery ? " AND": " ") + "p.stock >= :stockQuery"
//         replacements.stockQuery = stockQuery;
//     }
//     if (vendeurNameQuery) {
//         statement += (nameQuery || stockQuery ? " AND" : " ") + "v.name LIKE %:vendeurNameQuery%";
//         replacements.vendeurNameQuery = vendeurNameQuery;
//     }
//     // execution de la requete SQL (avec les options définies précedemment)
//     const [results, metadata] = await sequelize.query(statement,{
//         replacements: replacements,
//         type: QueryTypes.SELECT
//       });
//     //const produits = ???
//     res.json(produits);
// };

// find by id
export const findById = async (req, res, next) => {
    try {
        const produitId = req.params.id;
        const produit = await Produit.findByPk(produitId, { include: [ { model: Vendeur }] });
        if (produit)
            res.json(produit);
        else
            res.status(404).send("no product exist with id " + produitId);
    } catch (error) {
        next(error);
    }
};

// save
export const save = async (req, res, next) => {
    try {
        console.log(JSON.stringify(req.file));
        // recuperer le produit depuis le body de la requete
        let produit = req.body;
        produit.images = req.files.map(f => f.filename);
        produit.VendeurId = req.authenticatedUser.sub;
        // enregistrer le produit dans la db
        produit = await Produit.create(produit);
        // renvoyer le produit mis à jour
        res.json(produit);
    } catch (error) {
        req.files.forEach(f => unlink(f.path, (err) => {
            if (err) throw err;
        }));
        next(error);
    }
};

// update
export const update = async (req, res) => {
    const id = req.params.id;
    const newProduit = req.body;
    newProduit.images = JSON.parse(newProduit.images);
    const oldProduit = await Produit.findByPk(id);
    if (!oldProduit) {
        res.status(404).send("no product exist with id " + produitId);
        return;
    }
    oldProduit.images
        .filter(i => !newProduit.images?.includes(i))
        .forEach(i => unlink(
            process.env["files.path"] + "/" + i,
            (err) => { if (err) throw err; }));
    newProduit.images = [
        ...newProduit.images,
        ...req.files.map(f => f.filename)
    ];
    await oldProduit.update(newProduit);
    res.sendStatus(200);
};

// delete
export const remove = async (req, res) => {
    const produitId = req.params.id;
    const produit = await Produit.findByPk(produitId);
    if (produit) {
        produit.images.forEach(image => unlink(
            process.env["files.path"] + "/" + image, 
            err => { if (err) throw err; }));
        await produit.destroy();
        res.sendStatus(200);
    } else
        res.status(404).send("no product exist with id " + produitId);
};