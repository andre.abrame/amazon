import { Op } from "sequelize";
import { Vendeur } from "../models/vendeur.js";
import { Produit } from "../models/produit.js";

// find all
export const findAll = async (req, res) => {
    console.log(req.authenticatedUser.name)
    const vendeurs = await Vendeur.findAll();
    res.json(vendeurs);
};

// find by id
export const findById = async (req, res) => {
    const vendeurId = req.params.id;
    const vendeur = await Vendeur.findByPk(vendeurId);
    if (vendeur)
        res.json(vendeur);
    else
        res.status(404).send("no product exist with id " + vendeurId);
};

// save
export const save = async (req, res) => {
    // recuperer le vendeur depuis le body de la requete
    let vendeur = req.body;
    // enregistrer le vendeur dans la db
    vendeur = await Vendeur.create(vendeur);
    // renvoyer le vendeur mis à jour
    res.json(vendeur);
};

// update
export const update = async (req, res) => {
    const vendeurId = req.params.id;
    const updateData = req.body;
    const vendeur = await Vendeur.findByPk(vendeurId);
    if (vendeur) {
        await vendeur.update(updateData);
        res.sendStatus(200);
    } else
        res.status(404).send("no product exist with id " + vendeurId);
};

// delete
export const remove = async (req, res) => {
    const vendeurId = req.params.id;
    const vendeur = await Vendeur.findByPk(vendeurId);
    if (vendeur) {
        await vendeur.destroy();
        res.sendStatus(200);
    } else
        res.status(404).send("no product exist with id " + vendeurId);
};

export const findProduitsByVendeurId = async (req, res, next) => {
    const vendeurId = req.params.id;
    const produits = await Produit.findAll({
        include : {
            model: Vendeur,
            where: {
                id: vendeurId
            }
        }
    });
    res.json(produits);
};