import { Client } from "../models/client.js";
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { Vendeur } from "../models/vendeur.js";
import transporter from "../configs/mailer.config.js";
import crypto from 'crypto'

export const authenticate = async (req, res, next) => {
    try {
        const jwtRequest = req.body;
        let user;
        switch (jwtRequest.grantType) {
            case "password":
                const email = jwtRequest.username;
                const password = jwtRequest.password;
                // search user by email
                user = await Vendeur.findOne({ where: { "email": email, "activated": true } });
                if (!user)
                     user = await Client.findOne({ where: { "email": email, "activated": true } });
                if (!user) {
                    res.status(401).send({ message: "invalid username or password" });
                    return;
                }
                // validate user password
                if (!await bcrypt.compare(password, user.password)) {
                    res.status(401).send({ message: "invalid username or password" });
                    return;
                }
                // generate and return tokens
                res.json({
                    accessToken: generateAccessToken(user),
                    refreshToken: generateRefreshToken(user)
                });
                break;
            case "refreshToken":
                const { refreshToken } = jwtRequest;
                // validate and decode refresh token
                const decodedRefreshToken = jwt.verify(refreshToken, process.env['jwt.secret']);
                // search user
                const id =  decodedRefreshToken.sub;
                user = await Vendeur.findByPk(id);
                if (!user)
                    user = await Client.findByPk(id);
                if (!user) {
                    res.status(401).send({ message: "user not found" });
                    return;
                }
                // generate and return tokens
                res.json({
                    accessToken: generateAccessToken(user),
                    refreshToken: generateRefreshToken(user)
                });
                break;
            default:
                res.status(400).send({ message: "invalid grantType"});
        }
    } catch (error) {
        next(error);
    }
}


export const register = async (req, res, next) => {
    try {
        // recuperer l'utilisteur depuis le body de la requete
        let user = req.body;
        user.activated = false;
        user.token = crypto.randomBytes(20).toString('hex');
        user.password = await bcrypt.hash(user.password, 10);
        // enregistrer le produit dans la db
        if (user.type === "CLIENT")
            user = await Client.create(user);
        else if (user.type === "VENDEUR")
            user = await Vendeur.create(user);
        else {
            res.status(400).send("invalid user type");
            return;
        }
        // renvoyer le produit mis à jour
        res.json(user);
        // send confirmation email
        transporter.sendMail({
            from: 'formation.dwwm@laposte.net',
            to: user.email,
            subject: "Welcome to amazon !",
            html: `<h1>Welcome to amazon !</h1>
                   <p>Hello ${user.prenom}. Your account has been created. Click <a href='http://localhost:5173/activate/${user.token}'>here</a> to activate it.</p>`,
          });
    } catch (error) {
        next(error);
    }
}


export const activate =  async (req, res, next) => {
    try {
        // recuperer le token
        const token = req.params["token"]
        if (!token || token.length != 40)
            res.status(400).send({ message: "invalid token" });
        // search a client or vender associated with the token
        let user = await Vendeur.findOne({ where: { "token": token} });
        if (!user)
             user = await Client.findOne({ where: { "token": token} });
        if (!user) {
            res.status(400).send({ message: "invalid token" });
            return;
        }
        // activate user
        user.activated = true;
        user.token = null;
        // update database
        await user.save(user);
        res.sendStatus(200)
    } catch (error) {
        next(error);
    }
}


export const updatePassword = async (req, res, next) => {
    try {
        // recuperer l'utilisteur depuis le body de la requete
        let {id, oldPassword, newPassword } = req.body;
        // rechercher l'utilisateur dans la base de données
        let user = await Vendeur.findByPk(id);
        if (!user)
             user = await Client.findByPk(id);
        if (!user) {
            res.status(400).send({ message: "invalid user id" });
            return;
        }
        // verification de l'ancien mot de passe
        if (!await bcrypt.compare(oldPassword, user.password)) {
            res.status(400).send({ message: "invalid user password" });
            return;
        }
        // mise à jour du mot de passe dans la ase de données
        user.password = await bcrypt.hash(newPassword, 10);
        user.update();
        res.sendStatus(200);
    } catch (error) {
        next(error);
    }
}

const generateAccessToken = (user) => {
    return jwt.sign(
        { sub: user.id, name: user.nom, role: user.role },
        process.env['jwt.secret'],
        { expiresIn: 30}
    );
}

const generateRefreshToken = (user) => {
    return jwt.sign(
        { sub: user.id },
        process.env['jwt.secret'],
        { expiresIn: 60}
    );
}