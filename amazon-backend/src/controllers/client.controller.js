import { Client } from "../models/client.js";

// find all
export const findAllClients = async (req, res) => {
    const clients = await Client.findAll();
    res.json(clients);
};

// find by id
export const findClientById =  async (req, res) => {
    const clientId = req.params.id;
    const client = await Client.findByPk(clientId);
    if (client)
        res.json(client);
    else
        res.status(404).send("no client exist with id " + clientId);
};

// save
export const saveClient =  async (req, res) => {
    // recuperer le client depuis le body de la requete
    let client = req.body;
    client.role = 'CLIENT';
    // enregistrer le client dans la db
    client = await Client.create(client);
    // renvoyer le client mis à jour
    res.json(client);
};

// update
export const updateClient = async (req,res) => {
    const clientId = req.params.id;
    const updateData = req.body;
    const client = await Client.findByPk(clientId);
    if (client) {
        await client.update(updateData);
        res.sendStatus(200);
    } else
        res.status(404).send("no client exist with id " + clientId);
};

// delete
export const deleteClient = async (req,res) => {
    const clientId = req.params.id;
    const client = await Client.findByPk(clientId);
    if (client) {
        await client.destroy();
        res.sendStatus(200);
    } else
        res.status(404).send("no client exist with id " + clientId);
};