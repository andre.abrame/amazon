import './src/configs/dotenv.config.js'
import { sequelize } from './src/configs/db.config.js';
import { Client } from './src/models/client.js'
import { LignePanier } from './src/models/lignePanier.js'
import { Note } from './src/models/note.js'
import { Produit } from './src/models/produit.js';
import { Vendeur } from './src/models/vendeur.js';
import express from 'express';  
import { router as routerProduit } from './src/routers/produit.router.js'; // as permet ed e définir un alias : on importe router, mais localmeenrt il s'appelle routerProduit
import { router as routerClient } from './src/routers/client.router.js';
import { router as routerVendeur } from './src/routers/vendeur.router.js';
import { router as routerAuthentication } from './src/routers/authentication.router.js';
import { ValidationError } from 'sequelize';
import cors from 'cors';
import { jwtMiddleware } from './src/middlewares/jwt.middleware.js';
import { createServer } from 'http';

import { Server } from "socket.io";

import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express'
import jsonwebtoken from 'jsonwebtoken';
import { TokenExpiredErrorHandler, ValidationErrorHandler } from './src/middlewares/errorHandler.middleware.js';
const { TokenExpiredError } = jsonwebtoken;


// express initialization
const app = express();
// express middlewares
app.use(express.json());
app.use(cors());
app.use(function(req,res,next){setTimeout(next,1000)});
app.use(jwtMiddleware);
// express routers
app.use("/produits", routerProduit);
app.use("/clients", routerClient);
app.use("/vendeurs", routerVendeur);
app.use(routerAuthentication);
app.use('/files', express.static(process.env["files.path"]));
// error handling
app.use(ValidationErrorHandler);
app.use(TokenExpiredErrorHandler);

// synchronisation avec la base de données
await sequelize.sync();

// sockets
const httpServer = createServer(app);
const io = new Server(httpServer, { cors: { origin: '*' } });
const connectedSockets = [];
io.on('connection', (socket) => {
  connectedSockets.push(socket);
  console.log("SOCKET - vendeur connected");
  socket.on('disconnect', (code) => {
    connectedSockets.splice(connectedSockets.findIndex(s => s.id === socket.id), 1);
    console.log("SOCKET - vendeur disconnected");
  });
});


// swagger ui
const options = {
    definition: {
      openapi: '3.0.0',
      info: {
        title: 'Amazon',
        version: '1.0.0',
      },
    },
    apis: ['./src/routers/*.js'], // files containing annotations as above
  };
const openapiSpecification = swaggerJSDoc(options);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(openapiSpecification));

console.log(process.env["db.url"]);

httpServer.listen(3000, () => console.log("listening on port 3000"));
